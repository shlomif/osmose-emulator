###############################################################################
#
# * This file is part of Osmose Emulator, a Sega Master System/Game Gear
# * software emulator.
# *
# * Many thanks to Vedder Bruno, the original author of Osmose Emulator.
# *
# * Copyright holder 2001-2011 Vedder Bruno.
# * Work continued by 2016-2020 Carlos Donizete Froes [a.k.a coringao]
# *
# * Osmose Emulator is free software: you can redistribute it and/or modify it
# * under the terms of the GNU General Public License as published by the
# * Free Software Foundation, either version 3 of the License,
# * or (at your option) any later version.
#
###############################################################################

version: 1.6
  * Fixed the license notice in the source file.
  * Added desktop file.
  * Spelling errors fixed in file.
  * Updated manpage file: Added a new command to switch the menu bar.
  * Changed the release version (1.6).

version: 1.5
  * Unused source file debugger removed.
  * osmose-emulator.pro -- Created qmake project file and added complements.
  * OsmoseEmulator.pro -- Outdated qmake project file removed.
  * Fixed the license notice in the source file.
  * Implements toggleMenu feature. (Thanks Leandro Ramos)
  * Added gitlab-ci file.
  * Updated version 1.5 and year (2020).

version: 1.4
  * Set default sound device (Thanks to Kev-J)
  * Change sound buffer writing method (Thanks to Kev-J)
  * Qt Wayland widgets alternative in gnome-shell

version: 1.3
  * Replaced 'strncpy' with 'memcpy' to suppress the warning
  * Updated files version and year
  * Changed alsa device to "plughw"
  * Added to manpage in src directory
  * Changed the build flag to version 'gnu++17'

version: 1.2
  * Fixed bug in audio alsa
  * Fixed installation on architectures 'arm'

version: 1.1
  * Changed own icon in the main window
  * Changed image format: xpm to png
  * Added SVG image
  * Fixed spelling in some files
  * Copied the files from the directory (cpu) to the directory (src)
  * Updated file: OsmoseEmulator.pro
  * Some bug fixes
  * Fixed build for armel/armhf architectures
  * Added libraries required for armel/armhf architectures

version: 1.0
  * Created own icon in the main window
  * Replaced the QT version - 4.x to 5.x
  * Added in the main window menu - About
  * Revised common misspelled words in source code
  * Removed embedded copy of the source code 'unzip'

version: 0.9.96
  * Initial version
  * Acquired originally from the site and holder by copyright Bruno Vedder
